#pragma once

#include "Chip8System.hpp"


typedef void (*instr)(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key);


namespace c8
{
    static inline void
    c8_NOP(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
		Log::Info("Operation out of charts");
        return;
    }

    static inline void
    c8_0000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        switch (opcode & 0x000F) {
            case 0x0000: // 0x00E0: Clears the screen
                for (unsigned char &pix : sys.gfx)
                    pix = 0x0;
                drawFlag = true;
                pc += 2;
                break;

            case 0x000E: // 0x00EE: Returns from subroutine
                // Put the stored return address from the stack back into the program counter
                --sp;
                pc = sys.stack[sp];
                pc += 2;
                break;

            default:
                printf("Unknown opcode [0x0000]: 0x%X\n", opcode);
        }
    }

    static inline void
    c8_1000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        pc = opcode & 0x0FFF;
    }

    static inline void
    c8_2000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        sys.stack[sp] = pc;            // Store current address in stack
        ++sp;							// Increment stack pointer
        pc = opcode & 0x0FFF;
    }

    static inline void
    c8_3000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        if (sys.V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
            pc += 4;
        else
            pc += 2;
    }

    static inline void
    c8_4000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        if (sys.V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
            pc += 4;
        else
            pc += 2;

    }

    static inline void
    c8_5000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        if (sys.V[(opcode & 0x0F00) >> 8] == sys.V[(opcode & 0x00F0) >> 4])
            pc += 4;
        else
            pc += 2;

    }

    static inline void
    c8_6000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        sys.V[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
        pc += 2;

    }

    static inline void
    c8_7000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
    {
        sys.V[(opcode & 0x0F00) >> 8] += opcode & 0x00FF;
        pc += 2;

    }

	static inline void
	c8_8000_8000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[(opcode & 0x0F00) >> 8] = sys.V[(opcode & 0x00F0) >> 4];
		pc += 2;
	}
	static inline void
	c8_8000_8001(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[(opcode & 0x0F00) >> 8] |= sys.V[(opcode & 0x00F0) >> 4];
		pc += 2;
	}
	static inline void
	c8_8000_8002(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[(opcode & 0x0F00) >> 8] &= sys.V[(opcode & 0x00F0) >> 4];
		pc += 2;
	}
	static inline void
	c8_8000_8003(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[(opcode & 0x0F00) >> 8] ^= sys.V[(opcode & 0x00F0) >> 4];
		pc += 2;
	}
	static inline void
	c8_8000_8004(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		if (sys.V[(opcode & 0x00F0) >> 4] > (0xFF - sys.V[(opcode & 0x0F00) >> 8]))
			sys.V[0xF] = 1; //carry
		else
			sys.V[0xF] = 0;
		sys.V[(opcode & 0x0F00) >> 8] += sys.V[(opcode & 0x00F0) >> 4];
		pc += 2;
	}
	static inline void
	c8_8000_8005(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		if (sys.V[(opcode & 0x00F0) >> 4] > sys.V[(opcode & 0x0F00) >> 8])
			sys.V[0xF] = 0; // there is a borrow
		else
			sys.V[0xF] = 1;
		sys.V[(opcode & 0x0F00) >> 8] -= sys.V[(opcode & 0x00F0) >> 4];
		pc += 2;
	}
	static inline void
	c8_8000_8006(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[0xF] = sys.V[(opcode & 0x0F00) >> 8] & 0x1;
		sys.V[(opcode & 0x0F00) >> 8] >>= 1;
		pc += 2;
	}
	static inline void
	c8_8000_8007(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		if (sys.V[(opcode & 0x0F00) >> 8] > sys.V[(opcode & 0x00F0) >> 4])    // VY-VX
			sys.V[0xF] = 0; // there is a borrow
		else
			sys.V[0xF] = 1;
		sys.V[(opcode & 0x0F00) >> 8] = sys.V[(opcode & 0x00F0) >> 4] - sys.V[(opcode & 0x0F00) >> 8];
		pc += 2;
	}
	static inline void
	c8_8000_800E(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[0xF] = sys.V[(opcode & 0x0F00) >> 8] >> 7;
		sys.V[(opcode & 0x0F00) >> 8] <<= 1;
		pc += 2;
	}

	static instr opsubtable[0xE] = {
		c8_8000_8000,
		c8_8000_8001,
		c8_8000_8002,
		c8_8000_8003,
		c8_8000_8004,
		c8_8000_8005,
		c8_8000_8006,
		c8_8000_8007,
		c8_NOP,
		c8_NOP,
		c8_NOP,
		c8_NOP,
		c8_NOP,
		c8_8000_800E
	};
	static inline void
	c8_8000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		c8::opsubtable[(opcode & 0x000F)](opcode, pc, drawFlag, sp, sys, key);
	}

	static inline void
	c8_9000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		if (sys.V[(opcode & 0x0F00) >> 8] != sys.V[(opcode & 0x00F0) >> 4])
			pc += 4;
		else
			pc += 2;
	}

	static inline void
	c8_A000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.I = opcode & 0x0FFF;
		pc += 2;
	}
	static inline void
	c8_B000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		pc = (opcode & 0x0FFF) + sys.V[0];
	}
	static inline void
	c8_C000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		sys.V[(opcode & 0x0F00) >> 8] = (rand() % 0xFF) & (opcode & 0x00FF);
		pc += 2;
	}
	static inline void
	c8_D000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		unsigned short x = sys.V[(opcode & 0x0F00) >> 8];
		unsigned short y = sys.V[(opcode & 0x00F0) >> 4];
		unsigned short height = opcode & 0x000F;
		unsigned short pixel;

		sys.V[0xF] = 0;
		for (int yline = 0; yline < height; yline++) {
			pixel = sys.memory[sys.I + yline];
			for (int xline = 0; xline < 8; xline++) {
				if ((pixel & (0x80 >> xline)) != 0) {
					if (sys.gfx[(x + xline + ((y + yline) * 64))] == 1) {
						sys.V[0xF] = 1;
					}
					sys.gfx[x + xline + ((y + yline) * 64)] ^= 1;
				}
			}
		}

		drawFlag = true;
		pc += 2;
	}

	static inline void
	c8_E000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		switch (opcode & 0x00FF) {
		case 0x009E: // EX9E: Skips the next instruction if the key stored in VX is pressed
			if (key[sys.V[(opcode & 0x0F00) >> 8]] != 0)
				pc += 4;
			else
				pc += 2;
			break;

		case 0x00A1: // EXA1: Skips the next instruction if the key stored in VX isn't pressed
			if (key[sys.V[(opcode & 0x0F00) >> 8]] == 0)
				pc += 4;
			else
				pc += 2;
			break;

		default:
			printf("Unknown opcode [0xE000]: 0x%X\n", opcode);
		}
	}

	static inline void
	c8_F000(unsigned short &opcode, unsigned short &pc, bool &drawFlag, unsigned short &sp, System &sys, unsigned char* key)
	{
		switch (opcode & 0x00FF) {
		case 0x0007: // FX07: Sets VX to the value of the delay timer
			sys.V[(opcode & 0x0F00) >> 8] = sys.delay_timer;
			pc += 2;
			break;

		case 0x000A: // FX0A: A key press is awaited, and then stored in VX
		{
			bool keyPress = false;

			for (int i = 0; i < 16; ++i) {
				if (key[i] != 0) {
					sys.V[(opcode & 0x0F00) >> 8] = i;
					keyPress = true;
				}
			}

			// If we didn't received a keypress, skip this cycle and try again.
			if (!keyPress)
				return;

			pc += 2;
		}
		break;

		case 0x0015: // FX15: Sets the delay timer to VX
			sys.delay_timer = sys.V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x0018: // FX18: Sets the sound timer to VX
			sys.sound_timer = sys.V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x001E: // FX1E: Adds VX to I
			if (sys.I + sys.V[(opcode & 0x0F00) >> 8] >
				0xFFF)    // VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
				sys.V[0xF] = 1;
			else
				sys.V[0xF] = 0;
			sys.I += sys.V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x0029: // FX29: Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font
			sys.I = sys.V[(opcode & 0x0F00) >> 8] * 0x5;
			pc += 2;
			break;

		case 0x0033: // FX33: Stores the Binary-coded decimal representation of VX at the addresses I, I plus 1, and I plus 2
			sys.memory[sys.I] = sys.V[(opcode & 0x0F00) >> 8] / 100;
			sys.memory[sys.I + 1] = (sys.V[(opcode & 0x0F00) >> 8] / 10) % 10;
			sys.memory[sys.I + 2] = (sys.V[(opcode & 0x0F00) >> 8] % 100) % 10;
			pc += 2;
			break;

		case 0x0055: // FX55: Stores V0 to VX in memory starting at address I
			for (int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
				sys.memory[sys.I + i] = sys.V[i];

			// On the original interpreter, when the operation is done, I = I + X + 1.
			sys.I += ((opcode & 0x0F00) >> 8) + 1;
			pc += 2;
			break;

		case 0x0065: // FX65: Fills V0 to VX with values from memory starting at address I
			for (int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
				sys.V[i] = sys.memory[sys.I + i];

			// On the original interpreter, when the operation is done, I = I + X + 1.
			sys.I += ((opcode & 0x0F00) >> 8) + 1;
			pc += 2;
			break;

		default:
			printf("Unknown opcode [0xF000]: 0x%X\n", opcode);
		}
	}
    static instr optable[0x1F] = {
            c8_0000,
            c8_1000,
            c8_2000,
            c8_3000,
            c8_4000,
            c8_5000,
            c8_6000,
            c8_7000,
			c8_8000,
			c8_9000,
			c8_A000,
			c8_B000,
			c8_C000,
			c8_D000,
			c8_E000,
			c8_F000
    };
}
