#pragma once

#include <iostream>
#include <fstream>
#include <ctime>


struct System
{
    unsigned short I;
    unsigned char gfx[64 * 32];

    unsigned char V[16];
    unsigned short stack[16];
    unsigned char memory[4096];

    unsigned char delay_timer;
    unsigned char sound_timer;
};


class Chip8VirtualMachine
{
public:
	Chip8VirtualMachine() : m_RomInserted(false) {};

    ~Chip8VirtualMachine() = default;

    bool loadApplication(const std::string &filepath);

	void interpretCycle();

	void redirectCycle();

    unsigned char key[16];

    bool drawFlag;

    void updateTimers();

    void init();

	bool isRomInserted();

    System sys;

private:

    unsigned short pc;
    unsigned short sp;
    unsigned short opcode;

	bool m_RomInserted;
};
