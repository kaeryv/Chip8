

#include <Quark.h>

#include "Chip8Emulator.hpp"

namespace qk = quark;

int main()
{
  Log::SetLogLevel(Log::LevelInfo);
  Chip8Emulator* emu;
  emu = new Chip8Emulator();
  emu->start();
  delete emu; 
  return 0;
}
