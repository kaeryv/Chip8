#include "Chip8Emulator.hpp"

#include <filesystem>
#include <event/InputManager.h>
#include <utils/utils.hpp>
//#include <unistd.h>

Chip8Emulator::Chip8Emulator() : qk::Application()
{
}

Chip8Emulator::~Chip8Emulator()
{
}

const std::string Chip8Emulator::getDefaultName()
{
    return "Chip8Emulator";
}

const qk::WindowProperties Chip8Emulator::getDefaultWindowProperties()
{
    qk::WindowProperties temp;
    temp.fullscreen = false;
    temp.vsync = false;
    temp.height = 700;
    temp.width = 800;
    return temp;
}

void Chip8Emulator::debug()
{

}

void Chip8Emulator::ui()
{
    static float f = 540.0f;
    ImGui::Text("Chip8 Emulator using QuarK engine");
    ImGui::SliderFloat("Maximum UPS [Hz]", &f, 60.0, 800.);
	setMaxUPS(f);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", FrameTimeFromFrequency<Unit::ms>(ImGui::GetIO().Framerate),
                ImGui::GetIO().Framerate);

    ImGui::TextColored(ImVec4(1, 1, 0, 1), "List of roms:");
    ImGui::BeginChild("Scrolling");
	ImGui::Checkbox("Enable redirection", &redirectionEnabled);


    for (auto &p: std::filesystem::directory_iterator("roms")) {
        ImGui::Text("- ");
        ImGui::SameLine();
        if (ImGui::Button(p.path().string().c_str())) {
            chip->init();
            chip->loadApplication(p.path().string());
        }
    }
    ImGui::EndChild();

    //processInput();

	chip->updateTimers();
}

void Chip8Emulator::onTick() 
{
	glfwPollEvents();
	processInput();
	if (chip->isRomInserted())
		if (redirectionEnabled)
			chip->redirectCycle();
		else
			chip->interpretCycle();
		
	
	if (chip->drawFlag) 
	{
		chip->drawFlag = false;
		chipScreen->update(chip->sys.gfx);
	}
}
void Chip8Emulator::init()
{
    chip = new Chip8VirtualMachine();
    chip->init();
    qk::WavefontLoader loader;

    cube = loader.loadObjModel("./res/graphics/models/plane.obj");
    shader = new qk::Shader("./res/graphics/shaders/basic.glsl");
    shader->init();
    shader->loadInteger(0, 1);
    qk::Loader::Shaders.push_back(shader);

    texture = new qk::Texture("./res/textures/quark.png");
    texture->bind(0);
    //
    chipScreen = new qk::DynamicTexture(chip->sys.gfx, 64, 32, 1);
    chipScreen->bind(1);
    while (GLenum error = glGetError()) {
        Log::Fatal("gl initi error: " + std::to_string(error));
    }
}

void Chip8Emulator::processInput()
{
    if (qk::Input::isKeyDown(GLFW_KEY_ESCAPE))
        exit(0);
    chip->key[0x1] = (unsigned char) qk::Input::isKeyDown(QK_KEY_1);
    chip->key[0x2] = (unsigned char) qk::Input::isKeyDown(QK_KEY_2);
    chip->key[0x3] = (unsigned char) qk::Input::isKeyDown(QK_KEY_3);
    chip->key[0xC] = (unsigned char) qk::Input::isKeyDown(QK_KEY_4);
    chip->key[0x4] = (unsigned char) qk::Input::isKeyDown(QK_KEY_Q);
    chip->key[0x5] = (unsigned char) qk::Input::isKeyDown(QK_KEY_W);
    chip->key[0x6] = (unsigned char) qk::Input::isKeyDown(QK_KEY_E);
    chip->key[0xD] = (unsigned char) qk::Input::isKeyDown(QK_KEY_R);
    chip->key[0x7] = (unsigned char) qk::Input::isKeyDown(QK_KEY_A);
    chip->key[0x8] = (unsigned char) qk::Input::isKeyDown(QK_KEY_S);
    chip->key[0x9] = (unsigned char) qk::Input::isKeyDown(QK_KEY_D);
    chip->key[0xE] = (unsigned char) qk::Input::isKeyDown(QK_KEY_F);
    chip->key[0xA] = (unsigned char) qk::Input::isKeyDown(QK_KEY_Z);
    chip->key[0x0] = (unsigned char) qk::Input::isKeyDown(QK_KEY_X);
    chip->key[0xB] = (unsigned char) qk::Input::isKeyDown(QK_KEY_C);
    chip->key[0xF] = (unsigned char) qk::Input::isKeyDown(QK_KEY_V);
}
