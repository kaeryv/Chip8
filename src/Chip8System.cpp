#include "Chip8System.hpp"


#include <Log.hpp>

#include "instructions.h"


unsigned char chip8_fontset[80] =
        {
                0xF0, 0x90, 0x90, 0x90, 0xF0, //0
                0x20, 0x60, 0x20, 0x20, 0x70, //1
                0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
                0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
                0x90, 0x90, 0xF0, 0x10, 0x10, //4
                0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
                0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
                0xF0, 0x10, 0x20, 0x40, 0x40, //7
                0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
                0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
                0xF0, 0x90, 0xF0, 0x90, 0x90, //A
                0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
                0xF0, 0x80, 0x80, 0x80, 0xF0, //C
                0xE0, 0x90, 0x90, 0x90, 0xE0, //D
                0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
                0xF0, 0x80, 0xF0, 0x80, 0x80  //F
        };


void Chip8VirtualMachine::init()
{
    pc = 0x200;     // Program counter starts at 0x200 (Start adress program)
    opcode = 0;     // Reset current opcode
    sys.I = 0;      // Reset index register
    sp = 0;         // Reset stack pointer

    for (unsigned char &pix : sys.gfx)
        pix = 0;

    for (unsigned short &s : sys.stack)
        s = 0;

    for (int i = 0; i < 16; ++i)
        key[i] = sys.V[i] = 0;

    for (unsigned char &mem : sys.memory)
        mem = 0;

    for (int i = 0; i < 80; ++i)
        sys.memory[i] = chip8_fontset[i];

    sys.delay_timer = 0;
    sys.sound_timer = 0;

    drawFlag = true;

    srand((unsigned int) time(nullptr));
}

bool Chip8VirtualMachine::isRomInserted()
{
	return m_RomInserted;
}


void Chip8VirtualMachine::redirectCycle()
{
	// Fetch opcode
	opcode = sys.memory[pc] << 8 | sys.memory[pc + 1];

	// Process opcode
	c8::optable[(opcode & 0xF000) >> 12](opcode, pc, drawFlag, sp, sys, key);
	return;
}

void Chip8VirtualMachine::interpretCycle()
{
    // Fetch opcode
    opcode = sys.memory[pc] << 8 | sys.memory[pc + 1];
    
    // Process opcode
    switch (opcode & 0xF000) {
        case 0x0000:
            switch (opcode & 0x000F) {
                case 0x0000: // 0x00E0: Clears the screen
                    for (unsigned char &pix : sys.gfx)
                        pix = 0x0;
                    drawFlag = true;
                    pc += 2;
                    break;

                case 0x000E: // 0x00EE: Returns from subroutine
                    --sp;            // 16 levels of stack, decrease stack pointer to prevent overwrite
                    pc = sys.stack[sp];    // Put the stored return address from the stack back into the program counter
                    pc += 2;        // Don't forget to increase the program counter!
                    break;

                default:
                    printf("Unknown opcode [0x0000]: 0x%X\n", opcode);
            }
            break;

        case 0x1000: // 0x1NNN: Jumps to address NNN
            pc = opcode & 0x0FFF;
            break;

        case 0x2000: // 0x2NNN: Calls subroutine at NNN.
            sys.stack[sp] = pc;            // Store current address in stack
            ++sp;                    // Increment stack pointer
            pc = opcode & 0x0FFF;    // Set the program counter to the address at NNN
            break;

        case 0x3000: // 0x3XNN: Skips the next instruction if VX equals NN
            if (sys.V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
                pc += 4;
            else
                pc += 2;
            break;

        case 0x4000: // 0x4XNN: Skips the next instruction if VX doesn't equal NN
            if (sys.V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
                pc += 4;
            else
                pc += 2;
            break;

        case 0x5000: // 0x5XY0: Skips the next instruction if VX equals VY.
            if (sys.V[(opcode & 0x0F00) >> 8] == sys.V[(opcode & 0x00F0) >> 4])
                pc += 4;
            else
                pc += 2;
            break;

        case 0x6000: // 0x6XNN: Sets VX to NN.
            sys.V[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
            pc += 2;
            break;

        case 0x7000: // 0x7XNN: Adds NN to VX.
            sys.V[(opcode & 0x0F00) >> 8] += opcode & 0x00FF;
            pc += 2;
            break;

        case 0x8000:
            switch (opcode & 0x000F) {
                case 0x0000: // 0x8XY0: Sets VX to the value of VY
                    sys.V[(opcode & 0x0F00) >> 8] = sys.V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                    break;

                case 0x0001: // 0x8XY1: Sets VX to "VX OR VY"
                    sys.V[(opcode & 0x0F00) >> 8] |= sys.V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                    break;

                case 0x0002: // 0x8XY2: Sets VX to "VX AND VY"
                    sys.V[(opcode & 0x0F00) >> 8] &= sys.V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                    break;

                case 0x0003: // 0x8XY3: Sets VX to "VX XOR VY"
                    sys.V[(opcode & 0x0F00) >> 8] ^= sys.V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                    break;

                case 0x0004: // 0x8XY4: Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't
                    if (sys.V[(opcode & 0x00F0) >> 4] > (0xFF - sys.V[(opcode & 0x0F00) >> 8]))
                        sys.V[0xF] = 1; //carry
                    else
                        sys.V[0xF] = 0;
                    sys.V[(opcode & 0x0F00) >> 8] += sys.V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                    break;

                case 0x0005: // 0x8XY5: VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't
                    if (sys.V[(opcode & 0x00F0) >> 4] > sys.V[(opcode & 0x0F00) >> 8])
                        sys.V[0xF] = 0; // there is a borrow
                    else
                        sys.V[0xF] = 1;
                    sys.V[(opcode & 0x0F00) >> 8] -= sys.V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                    break;

                case 0x0006: // 0x8XY6: Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift
                    sys.V[0xF] = sys.V[(opcode & 0x0F00) >> 8] & 0x1;
                    sys.V[(opcode & 0x0F00) >> 8] >>= 1;
                    pc += 2;
                    break;

                case 0x0007: // 0x8XY7: Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't
                    if (sys.V[(opcode & 0x0F00) >> 8] > sys.V[(opcode & 0x00F0) >> 4])    // VY-VX
                        sys.V[0xF] = 0; // there is a borrow
                    else
                        sys.V[0xF] = 1;
                    sys.V[(opcode & 0x0F00) >> 8] = sys.V[(opcode & 0x00F0) >> 4] - sys.V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                    break;

                case 0x000E: // 0x8XYE: Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift
                    sys.V[0xF] = sys.V[(opcode & 0x0F00) >> 8] >> 7;
                    sys.V[(opcode & 0x0F00) >> 8] <<= 1;
                    pc += 2;
                    break;

                default:
                    printf("Unknown opcode [0x8000]: 0x%X\n", opcode);
            }
            break;

        case 0x9000: // 0x9XY0: Skips the next instruction if VX doesn't equal VY
            if (sys.V[(opcode & 0x0F00) >> 8] != sys.V[(opcode & 0x00F0) >> 4])
                pc += 4;
            else
                pc += 2;
            break;

        case 0xA000: // ANNN: Sets I to the address NNN
            sys.I = opcode & 0x0FFF;
            pc += 2;
            break;

        case 0xB000: // BNNN: Jumps to the address NNN plus V0
            pc = (opcode & 0x0FFF) + sys.V[0];
            break;

        case 0xC000: // CXNN: Sets VX to a random number and NN
            sys.V[(opcode & 0x0F00) >> 8] = (rand() % 0xFF) & (opcode & 0x00FF);
            pc += 2;
            break;

        case 0xD000: // DXYN: Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels.
            // Each row of 8 pixels is read as bit-coded starting from memory location I;
            // I value doesn't change after the execution of this instruction.
            // VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn,
            // and to 0 if that doesn't happen
        {
            unsigned short x = sys.V[(opcode & 0x0F00) >> 8];
            unsigned short y = sys.V[(opcode & 0x00F0) >> 4];
            unsigned short height = opcode & 0x000F;
            unsigned short pixel;

            sys.V[0xF] = 0;
            for (int yline = 0; yline < height; yline++) {
                pixel = sys.memory[sys.I + yline];
                for (int xline = 0; xline < 8; xline++) {
                    if ((pixel & (0x80 >> xline)) != 0) {
                        if (sys.gfx[(x + xline + ((y + yline) * 64))] == 1) {
                            sys.V[0xF] = 1;
                        }
                        sys.gfx[x + xline + ((y + yline) * 64)] ^= 1;
                    }
                }
            }

            drawFlag = true;
            pc += 2;
        }
            break;

        case 0xE000:
            switch (opcode & 0x00FF) {
                case 0x009E: // EX9E: Skips the next instruction if the key stored in VX is pressed
                    if (key[sys.V[(opcode & 0x0F00) >> 8]] != 0)
                        pc += 4;
                    else
                        pc += 2;
                    break;

                case 0x00A1: // EXA1: Skips the next instruction if the key stored in VX isn't pressed
                    if (key[sys.V[(opcode & 0x0F00) >> 8]] == 0)
                        pc += 4;
                    else
                        pc += 2;
                    break;

                default:
                    printf("Unknown opcode [0xE000]: 0x%X\n", opcode);
            }
            break;

        case 0xF000:
            switch (opcode & 0x00FF) {
                case 0x0007: // FX07: Sets VX to the value of the delay timer
                    sys.V[(opcode & 0x0F00) >> 8] = sys.delay_timer;
                    pc += 2;
                    break;

                case 0x000A: // FX0A: A key press is awaited, and then stored in VX
                {
                    bool keyPress = false;

                    for (int i = 0; i < 16; ++i) {
                        if (key[i] != 0) {
                            sys.V[(opcode & 0x0F00) >> 8] = i;
                            keyPress = true;
                        }
                    }

                    // If we didn't received a keypress, skip this cycle and try again.
                    if (!keyPress)
                        return;

                    pc += 2;
                }
                    break;

                case 0x0015: // FX15: Sets the delay timer to VX
                    sys.delay_timer = sys.V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                    break;

                case 0x0018: // FX18: Sets the sound timer to VX
                    sys.sound_timer = sys.V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                    break;

                case 0x001E: // FX1E: Adds VX to I
                    if (sys.I + sys.V[(opcode & 0x0F00) >> 8] >
                        0xFFF)    // VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
                        sys.V[0xF] = 1;
                    else
                        sys.V[0xF] = 0;
                    sys.I += sys.V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                    break;

                case 0x0029: // FX29: Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font
                    sys.I = sys.V[(opcode & 0x0F00) >> 8] * 0x5;
                    pc += 2;
                    break;

                case 0x0033: // FX33: Stores the Binary-coded decimal representation of VX at the addresses I, I plus 1, and I plus 2
                    sys.memory[sys.I] = sys.V[(opcode & 0x0F00) >> 8] / 100;
                    sys.memory[sys.I + 1] = (sys.V[(opcode & 0x0F00) >> 8] / 10) % 10;
                    sys.memory[sys.I + 2] = (sys.V[(opcode & 0x0F00) >> 8] % 100) % 10;
                    pc += 2;
                    break;

                case 0x0055: // FX55: Stores V0 to VX in memory starting at address I
                    for (int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
                        sys.memory[sys.I + i] = sys.V[i];

                    // On the original interpreter, when the operation is done, I = I + X + 1.
                    sys.I += ((opcode & 0x0F00) >> 8) + 1;
                    pc += 2;
                    break;

                case 0x0065: // FX65: Fills V0 to VX with values from memory starting at address I
                    for (int i = 0; i <= ((opcode & 0x0F00) >> 8); ++i)
                        sys.V[i] = sys.memory[sys.I + i];

                    // On the original interpreter, when the operation is done, I = I + X + 1.
                    sys.I += ((opcode & 0x0F00) >> 8) + 1;
                    pc += 2;
                    break;

                default:
                    printf("Unknown opcode [0xF000]: 0x%X\n", opcode);
            }
            break;

        default:
            printf("Unknown opcode: 0x%X\n", opcode);
    }

}

bool Chip8VirtualMachine::loadApplication(const std::string &filename)
{
    init();
    Log::Info("Loading: " + filename);

    // Open file
    FILE *pFile = fopen(filename.c_str(), "rb");
    if (pFile == nullptr) {
        fputs("File error", stderr);
        return false;
    }

    // Check file size
    fseek(pFile, 0, SEEK_END);
    long lSize = ftell(pFile);
    rewind(pFile);
    printf("Filesize: %d\n", (int) lSize);

    // Allocate memory to contain the whole file
    char *buffer = (char *) malloc(sizeof(char) * lSize);
    if (buffer == nullptr) {
        fputs("Memory error", stderr);
        return false;
    }

    // Copy the file into the buffer
    size_t result = fread(buffer, 1, lSize, pFile);
    if (result != (size_t) lSize) {
        fputs("Reading error", stderr);
        return false;
    }

    // Copy buffer to Chip8 memory
    if ((4096 - 512) > lSize) {
        for (int i = 0; i < lSize; ++i)
            sys.memory[i + 512] = buffer[i];
    } else
        printf("Error: ROM too big for memory");

    fclose(pFile);
    free(buffer);

	m_RomInserted = true;

    return true;
}

void Chip8VirtualMachine::updateTimers()
{

    if (sys.delay_timer > 0)
        --sys.delay_timer;

    if (sys.sound_timer > 0) {
        if (sys.sound_timer == 1) {
            Log::Info("beep");
        }

        --sys.sound_timer;
    }
}
