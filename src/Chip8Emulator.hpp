#pragma once

#include <Quark.h>

#include "config.h"

#include "Chip8System.hpp"

namespace qk = quark;

class Chip8Emulator : public qk::Application
{
public:
    Chip8Emulator();

    ~Chip8Emulator() override;

public:
    void init() override;

    void debug() override;

    void ui() override;

	void onTick() override;

    void processInput();

    const qk::WindowProperties getDefaultWindowProperties() override;

    const std::string getDefaultName() override;

private:
    qk::RawModel *cube;
    qk::Shader *shader;
    qk::Texture *texture;
    qk::DynamicTexture *chipScreen;

    Chip8VirtualMachine *chip;

	bool redirectionEnabled;
};
